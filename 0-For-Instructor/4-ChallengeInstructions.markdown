# 303: Effortless Responsiveness with AsyncDisplayKit, Part 4: Challenge Instructions

We've actually been using self sizing nodes this whole time. It's time to take it to another level.

There are a ton of things that can slow down a collection view, keeping the collection view from reaching 60 frames per second during scroll. Text layout and display is one of them. So one technique to avoid this bottleneck is to hide the text until the user needs it, like we did in the lab. But ideally we could just display the text right under the image, giving the app a more magazine-like feel.

## A: Self Sizing Node Cells

Your challenge, should you choose to accept it, is to rebuild the CardNode from the lab so that the card displays the description text below the image. Each node should be sized precisely to fit its contents (one image and a paragraph of text).

By the time you have finished this challenge you will have mastered the first step towards asynchronous display, the art of node hierarchy construction.

![](./4-ChallengeImages/Challenge_1.png)

Go ahead and open **ChallengeViewController.swift**. It's the same view controller you just implemented in the lab except it's using a different CellNode class. Build and run. Notice how the collection view in the Challenge tab looks exactly the same as the one in the Demo tab. That's your starting point. Before looking at the hints below, try to build the new layout and think through what all has to change. :]

### Hints
1. You don't need the more button nor the blurred image
2. Now that the blur node is gone, the description node needs to be added to the card node
3. Next part is key, `calculateSizeThatFits` needs to
  A. Measure the description text node with a constrained **width** that equals the image's width minus 40 point padding
  B. Measure the description text node with a constrained **height** that equals the constrained size passed into the method
  C. Return a size, specifically a height, that is based on the image and description node measurement
  D. Return a size, specifically a width, that is baded on the image's measured width
4. The description node needs to have it's frame calculated in the FrameSet class


If you get this working, congrats – you've earned the master node craftsman title! :]

## B: Detail View

This next challenge involves creating a new view controller and ASDisplayNode subclass from scratch. Each card has additional details that should be presented full screen:

![](./4-ChallengeImages/Challenge_2.png)

Think through all the necessary steps that need to occur in order to present a full screen view controller in response to a collection view cell selection. Here are some important tips:

* The modal transition style should be `CrossDissolve`
* Use `NSAttributedString.attributedStringForSubtitleText("")` to create the location attributed string
* The Card data model has properties for detail description, location, and year build

As in the last challenge, try completing the challenge before looking at the hints below.

### Hints
1. `ASCollectionViewDelegate` is your friend
2. Try presenting an empty view controller in `collectionView(collectionView: ASCollectionView!, didSelectItemAtIndexPath indexPath: NSIndexPath!)`
3. The new view controller will want to have the card data model object, so will your node subclass
4. Go ahead and build the new `ASDisplayNode` subclass before finishing the view controller
5. Yup, set the background blur image node's content mode to `.ScaleAspectFill`
6. The blur image node's image modification block property needs to be set in order to display a blurred image
7. Remember the helper computed property `blurClosure` that returns a closure that should be used to set the image modification block property (this helper computed property can be accessed from the blur node instance itself)
8. Since this node is full-screen `calculateSizeThatFits(constrainedSize: CGSize)` should simply return the passed in size, callers should pass in the screen's size
9. Don't forget to measure all your subnodes (except for the background blur) in `calculateSizeThatFits(constrainedSize: CGSize)`
10. Also don't forget to calculate frames in `layout()` (don't worry about creating a frameset class for this node)
11. Eeek, the node has to trigger a view controller dismissal for the close button. Closures are your friend
12. More specifically, you can have an exit closure property on the node subclass that the view controller can set with the correct dismissal logic
13. The new view controller sure is different. It probably wants to wait until `viewWillLayoutSubviews()` before constructing its node
14. That's because the view controller won't know its root view's frame until `viewWillLayoutSubviews()` and you'll want to pass in the view's frame size into `measure()`
15. Don't forget to give the node an exit closure
16. This is **important**. In the node subclass, make sure to call `recursivelyReclaimMemory()` before calling the exit closure or else you'll hang on to memory you wish you hadn't

OK. Now you *really* know what's up. You've built a view controller and its root node's subclass entirely from scratch! Not so fast... When tapping on a cell, the background blur appears a second or two after the view controller is presented. Wouldn't it be nice to fade in the blur along with all the text (except for the title)?

## C: Smooth Blur Appearance

In this challenge you'll implement `subnodeDisplayDidFinish(subnode: ASDisplayNode!)`. This method is useful for performing any steps in response to a subnode finishing drawing. For example, in this challenge the method will be used to nicely fade-in the blur alongside the contents. For the last and final challenge follow these steps:

1. Hide the location, description, and year text nodes when setting them up
2. Also hide the background blur node during setup
2. In the detail `ASDisplayNode` subclass, implement `subnodeDisplayDidFinish(subnode: ASDisplayNode!)`, don't forget to call super!
3. If the passed in subnode is the background blur, animate in (0.5 secondds) the background along with the text nodes

Doesn't that feel much nicer! The view controller is presented immediately with the title to let the user know they tapped on the right cell. Then, once the blur operation is complete, the rest of the content shows up. Now that you can layout and display asynchronously, remember that it's important to present the user with a super fluid experience especially while display is occurring in the background and the user is in a waiting state.

Congratulations! You're now ready to start using AsyncDisplayKit out in the real world. And don't forget: friends don't let friends display irresponsively.
