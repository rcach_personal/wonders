# 303: Effortless Responsiveness with AsyncDisplayKit, Part 2: Demo Instructions

In this demo, you will build an asynchronous UI and you will take the first step in mastering AsyncDisplayKit.
The steps here will be explained in the demo, but here are the raw steps in case you miss a step or get stuck.

## 0) How to Set Up a Project _(Already Completed in Project)_

Create Xcode Project

Include AsyncDisplayKit via CocoaPods

Open Workspace and add bridging header

In the briding header, import <AsyncDisplayKit/AsyncDisplayKit.h>

## 1) Display node on screen

Instantiate an image node inside of `viewDidLoad()` in **DemoViewController.swift**:

    let imageNode = ASImageNode()

Provide an image for the node:

    imageNode.image = UIImage(named:"0")

Measure the node's size _(use the screen size as the constrained size)_:

    imageNode.measure(UIScreen.mainScreen().bounds.size)

Frame the node at 0,0 _(upper left-hand corner)_:

    imageNode.frame = CGRect(origin: CGPointZero, size: imageNode.calculatedSize)

Add the node's view to the view controller's root view:

    self.view.addSubview(imageNode.view)

Build and run

## 2) Explore how this is better than UIKit

Before measuring, give the node an image modification closure that has a four second sleep:

    imageNode.imageModificationBlock = { originalImage in
      sleep(4)
      return originalImage
    }

To simulate `UIImageView`, set `imageNode`'s `displaysAsynchronously` property to `false`:

    imageNode.displaysAsynchronously = false

Build and run, notice how this _blocks the main thread_

Set `displaysAsynchronously` back to true:

    imageNode.displaysAsynchronously = true

Build and run, notice how this does _not_ block the main thread and how the app no longer takes time to open

Wrap all the lines inside of a dispatch async to a background queue:

    dispatch_async(dispatch_queue_t.backgroundQueue) {
      let imageNode = ASImageNode()
      imageNode.displaysAsynchronously = true
      imageNode.image = UIImage(named:"0")
      imageNode.imageModificationBlock = { originalImage in
        sleep(4)
        return originalImage
      }
      imageNode.measure(UIScreen.mainScreen().bounds.size)
      imageNode.frame = CGRect(origin: CGPointZero, size: imageNode.calculatedSize)
      self.view.addSubview(imageNode.view)
    }

Wrap the line that adds the image node's view inside of a dispatch async to the main queue:

    dispatch_async(dispatch_queue_t.mainQueue) {
        self.view.addSubview(imageNode.view)
    }

Build and run.

## 3) Build node hierarchy

Delete the outer dispatch async including all it's contents.

Also delete `addDisplayLink()`.

Open **CardNode.swift** and give tour

Inside `init!(card: Card)` in the **Set Up Nodes** section give the title text node an attributed string:

    titleTextNode.attributedString = NSAttributedString.attributedStringForTitleText(card.name)

At the end of the **Build Hierarchy** section, add the title text node as a subnode:

    addSubnode(titleTextNode)

 Inside `calculateSizeThatFits(constrainedSize: CGSize)` use the card size to measure the title text node:

    titleTextNode.measure(cardSize)

Inside `layout()` frame the title text node using the frame set:

    titleTextNode.frame = frames.titleFrame

 That's all we need to add the title on top of the image. Go back to **DemoViewController.swift**

 Instantiate, set up, measure, frame and add a CardNode to the screen:

     dispatch_async(dispatch_queue_t.backgroundQueue) {
      let node = CardNode(card: card)
      node.measure(UIScreen.mainScreen().bounds.size)
      let origin = UIScreen.mainScreen().bounds.originForCenteredRectWithSize(node.calculatedSize)
      node.frame = CGRect(origin: origin, size: node.calculatedSize)

      dispatch_async(dispatch_get_main_queue()) {
        self.view.addSubview(node.view)
      }
    }

 Build and run

## 6) That's it!

Congrats, at this time you should have an asynchronous display UI, and learned a lot about the basics of AsyncDisplayKit along the way! You are ready to move on to the lab.
